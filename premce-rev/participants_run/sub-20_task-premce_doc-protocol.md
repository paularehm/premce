# PrEMCE. Protocol.

## ID: __________________________________

Day 1 |--------------------- | Day 2 | --------------------- | 
| ----- | ----- | ----- | ----- | 
Date: |				| Date: | 				
Starting time: | 	| Starting time: | 		
Ending time: |		| Ending time: |		

## Before Session on Day 1
### Documents available
- [ ] Signed **Informed consent**.
- [ ] Participant's **experiment links**.
- [ ] Participant's **session protocol**.
- [ ] Started a meeting in jitsi.

## During Session on Day 1
- [ ] Ask about general well-being. Comments?:
- [ ] The participant is using a desktop computer or a laptop on a desk/table.
- [ ] The participant is sitting down on a chair.
- [ ] The participant is on a quiet room.
- [ ] The participant is alone in the room.

In general, I would say that the participant is in an appropriate environment. 
- [ ] Yes
- [ ] No. Comments: The participant complained about...


### ses 01 - part 01
- [ ] Send **ses 01 - part 01** link.

Understood the task?
- [ ] Yes
- [ ] No. Comments:

- [ ] At the end of the practice trials, invite participant to read the percentage. 
- [ ] If performance is below 80%, provide explanation about the task again and invite participant to do the task again by pressing "j"
- [ ] If participants performance is at least 80%, invite participant to press "n" to continue with the task, otherwise repeat previous point. 
- [ ] After the practice trials, at the end of the first block ask participant to read the percentage on the screen. If satisfactory, invite participant to press "n" to continue, otherwise provide info and ask participant to press "j" to do the task again

## Before Session on Day 2
### Documents available
- [ ] Participant's **experiment links**.
- [ ] Participant's **session protocol**.
- [ ] Started a meeting in jitsi.

## During Session on Day 2
- [ ] Ask about general well-being. Comments?:
- [ ] The participant is using a desktop computer or a laptop on a desk/table.
- [ ] The participant is sitting down on a chair.
- [ ] The participant is on a quiet room.
- [ ] The participant is alone in the room.

In general, I would say that the participant is in an appropriate environment. 
- [ ] Yes
- [ ] No. Comments:

### Phase 2
- [ ] Send **ses 02 - part 01** link.

Understood the task?
- [ ] Yes
- [ ] No. Comments:

### Phase 3
- [ ] Send **ses 02 - part 02** link.

Understood the task?
- [ ] Yes
- [ ] No. Comments:

