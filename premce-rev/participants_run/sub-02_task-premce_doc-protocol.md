# PrEMCE. Protocol.

## ID: ________________RGTA2022__________________

Day 1 |--------------------- | Day 2 | --------------------- | 
| ----- | ----- | ----- | ----- | 
Date: |	22/01/2021			| Date: | 	23/01/2021			
Starting time: | 10:00	| Starting time: | 	10:00	
Ending time: |	10:30	| Ending time: |		

## Before Session on Day 1
### Documents available
- [x] Signed **Informed consent**.
- [x] Participant's **experiment links**.
- [x] Participant's **session protocol**.
- [x] Started a meeting in jitsi.

## During Session on Day 1
- [x] Ask about general well-being. Comments?:
- [x] The participant is using a desktop computer or a laptop on a desk/table.
- [x] The participant is sitting down on a chair.
- [x] The participant is on a quiet room.
- [x] The participant is alone in the room.

In general, I would say that the participant is in an appropriate environment. 
- [x] Yes
- [ ] No. Comments: The participant complained about...


### ses 01 - part 01
- [x] Send **ses 01 - part 01** link.

Understood the task?
- [x] Yes
- [ ] No. Comments:

- [x] At the end of the practice trials, invite participant to read the percentage. 
- [x] If performance is below 80%, provide explanation about the task again and invite participant to do the task again by pressing "j"
- [x] If participants performance is at least 80%, invite participant to press "n" to continue with the task, otherwise repeat previous point. **participant did the practice task again**
- [x] After the practice trials, at the end of the first block ask participant to read the percentage on the screen. If satisfactory, invite participant to press "n" to continue, otherwise provide info and ask participant to press "j" to do the task again
- comments : participant said she struggled with understanding the categories of some objects. Especially she was confused between electronic devices and outdoor and sports objects. 
She did not manage to go to an acceptable percentage at the end of the first task. 

## Before Session on Day 2
### Documents available
- [x] Participant's **experiment links**.
- [x] Participant's **session protocol**.
- [x] Started a meeting in jitsi.

## During Session on Day 2
- [x] Ask about general well-being. Comments?:
- [x] The participant is using a desktop computer or a laptop on a desk/table.
- [x] The participant is sitting down on a chair.
- [x] The participant is on a quiet room.
- [x] The participant is alone in the room.

In general, I would say that the participant is in an appropriate environment. 
- [x] Yes
- [ ] No. Comments:

### Phase 2
- [x] Send **ses 02 - part 01** link.

Understood the task?
- [x] Yes
- [ ] No. Comments:

**An error message occurred: not able to download participants' file"**

### Phase 3
- [x] Send **ses 02 - part 02** link.

Understood the task?
- [x] Yes
- [ ] No. Comments:

