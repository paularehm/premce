# PrEMCE. Protocol.

## ID: ADLA2009__________________________________

Day 1 |--------------------- | Day 2 | --------------------- | 
| ----- | ----- | ----- | ----- | 
Date: |	05/02/2021			| Date: | 06/02/2021 				
Starting time: | 17:30	| Starting time: | 	13:00	
Ending time: | 18:12		| Ending time: | 14:04		

## Before Session on Day 1
### Documents available
- [x] Signed **Informed consent**.
- [x] Participant's **experiment links**.
- [x] Participant's **session protocol**.
- [x] Started a meeting in jitsi.

## During Session on Day 1
- [x] Ask about general well-being. Comments?: No
- [x] The participant is using a desktop computer or a laptop on a desk/table.
- [x] The participant is sitting down on a chair.
- [x] The participant is on a quiet room.
- [x] The participant is alone in the room.

In general, I would say that the participant is in an appropriate environment. 
- [x] Yes
- [ ] No. Comments: The participant complained about...


### ses 01 - part 01
- [x] Send **ses 01 - part 01** link.

Understood the task?
- [x] Yes
- [ ] No. Comments:

- [x] At the end of the practice trials, invite participant to read the percentage. 
- [x] If performance is below 80%, provide explanation about the task again and invite participant to do the task again by pressing "j"
- [ ] If participants performance is at least 80%, invite participant to press "n" to continue with the task, otherwise repeat previous point. 
- [x] After the practice trials, at the end of the first block ask participant to read the percentage on the screen. If satisfactory, invite participant to press "n" to continue, otherwise provide info and ask participant to press "j" to do the task again

Participant couldn't reach 80% after three repititions of the practice trials (60%, 60%, 50%). Said the task was too fast.
She also noted that the objects were sometimes ambiguous and couldn't be clearly assigned to one category (e.g. electronic device / tools)

## Before Session on Day 2
### Documents available
- [x] Participant's **experiment links**.
- [x] Participant's **session protocol**.
- [x] Started a meeting in jitsi.

## During Session on Day 2
- [x] Ask about general well-being. Comments?: No
- [x] The participant is using a desktop computer or a laptop on a desk/table.
- [x] The participant is sitting down on a chair.
- [x] The participant is on a quiet room.
- [x] The participant is alone in the room.

In general, I would say that the participant is in an appropriate environment. 
- [x] Yes
- [ ] No. Comments:

### Phase 2
- [x] Send **ses 02 - part 01** link.

Understood the task?
- [x] Yes
- [ ] No. Comments:

Comments: Participant noted that some objects were difficult to identify and she wasn't always sure what was presented. Therefore she also had some difficulties assigning the objects to the categories.
She also said that this time the task was easier for her because she noticed that the corresponding numbers for the monsters (1 and 2) changed and she focused on that. During Part 1 she thought that the numbers/the order of the monsters remained constant (due to vague instruction). That would explain her low performance in Part 1.

### Phase 3
- [x] Send **ses 02 - part 02** link.

Understood the task?
- [x] Yes
- [ ] No. Comments:

