# Methods

Trial structure is adapted by Bosch et al., 2014. We have two characters per trial so there is no sensory information spilled during encoding and what is recalled is participants reinstatement of the character. 

Probabilities for the contingencies were set at 75% for the preferred choice (the category that each character would prefer most of the time), and 25 % for the rest, distributed equally among the remining categories (0.08333 each). 

Categories chosen for practice were "Gamestoy & entertainment", "Clothing", "Vehicle", "Stationary & school supply". 


The first 40 images with the highest category agreement were selected for each category

In order to counterbalance within participants: 4 categories, that can appear in 4 different locations. The permutations are four factorial: 24. 
We need it to repeat them two times for characters and two times for the cue (appearing first or second). In total, 96 trials (48 per character). 

For the practice block, 60 trials were selected. 

- For the task, the categories were selected among the ones with more exemplars. Food was discarded because of its superior image agreement scores, compared to the other categories. Bulding infrastrucure was discarded because of it was not as common as the other ones. 
The trials were permutated as in the practice task. As we have 24 per character per block, 24 trials were sampled from the 48 for each sample. 
For each list (A, Aplus, B, C, D), each character is presented associated with an object category 18 times, while 6 times associated with a different category (2 times each of the category). 
For each category, 48 objects were selected (36 for congruent, 12 for incongruent - see ppt for further clarifications), to be displayed on day 2. 36 objects were then assigned to recognition set as "new" objects, and the rest were used as fillers (day1)

Five lists were created: List A (consolidated over night). List A plus, with the same contingencies as in listA. In list B, the contingencies change for both characters to new contingencies. the preferred categories for both characters were never preferred before. In list C, the contingencies changed so that for character red, the preferred category was the one that was previously preferred by black character. For character black, the preferred category was the same to list A (switch back to over night consolidated). In list D, the preferred category for character red was back to the one preferred in list A (consolidated), while for the black character the preferred one was the one preferred by character red in list B.


In selecting the objects, some of them were controversial. Some of them were repeated with different images ("djmixer01","cleaver02","fork07b","jar03","mug05", "pot02a", "strainer02", "muffintray02")

Some others were considered ambiguous by Rasmus ("sandpaper, paint scraper, musical wooden spoons, ice scraper, hose nozzle, car battery, bag tie, backfloat")


the objects selected for each category where not significantly different among each other, for what concerns image agreement, for day2 and recognition, and day1 list. 

for each image category, 36 images were randomly assigned to the images shown on Day2, an 36 images were selected as new images for recognition test. In addition, 12 images were selected as fillers for day2 (25% times trials - incongruent trials). The remaining images, were selected as images for Day1. As for most categories there were not enough images, some images were repeated in day 1 and in the warmup task. 
So we are testing only the images of the congruent trials. 

In the recognition task, an association task were added, conditional on participants recognizing an object as old. Participants have to indicate the monster each object was associated to at encoding. 
