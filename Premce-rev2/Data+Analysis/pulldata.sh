#!/bin/bash

# get current folder
wd="/home/francesco/PowerFolders/Frankfurt_University/collaboration_with_Berlin_Group/Pilots/Premce-rev2"

# assign directories' names
folders="premceCat2_ses-01_part-01_label-phase1 premceCat2_ses-02_part-01_label-phase1 
premceCat2_ses-02_part-02_label-phase2"

for f in $folders
do

# go to the first directory
cd $wd/task/$f

pwd

# pull data
git pull

# synchronize with the working folder
rsync -v -r $wd/task/$f/data/ \
$wd/testing/gitLab/premce/Premce-rev2/Data+Analysis/raw_data/

done