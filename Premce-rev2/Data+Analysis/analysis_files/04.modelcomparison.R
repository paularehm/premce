# model comparison: compare fit
# this script compare the BIC of the different models

library(lme4)
library(lmerTest)
library(car)
library(reshape2)
library(ggplot2)
library(dplyr)
library(ggjoy)

rm(list=ls())
cd<-getwd()


# retrieve the different models
setwd("output_files")
names<-list.files(pattern= "estimated_*")

# create variable to stor model name
modelname<-vector()
# counter to store the different mdoel names
count<-1

# loop through the names
for (name in names){
  modelname[count]<-substr(name, 22, nchar(name)-4)
  assign(modelname[count], read.csv(name))
  count<-count+1
}

Sub<-nrow(get(modelname))

# now create a dataframe to summarise them all
BicAll<-data.frame(matrix(NA, nrow=Sub, ncol=0))

# create number of subjects
BicAll$Sub<-seq(1:Sub)

for (name in modelname){
  BicAll[[name]]<-get(name)$BIC
}

# get a summary
summary(BicAll[2:ncol(BicAll)])

par(mfrow=c(3,3))
for (v in 2:ncol(BicAll)){
  hist(BicAll[, v], main = names(BicAll)[v])
}
par(mfrow=c(1,1))

BicAllmelt<-melt(BicAll, id.vars = "Sub")
names(BicAllmelt)<-c("Sub", "model", "BIC")

BicMod<-lmer(BIC~model+(1|Sub), data = BicAllmelt)
summary(BicMod)

# summary by model
BicAllmelt %>%
  group_by(model)   %>%
  summarize(Mean = mean(BIC, na.rm=TRUE))

# plot them
p <- ggplot(BicAllmelt, aes(model, BIC))
p+
  geom_violin()+
  geom_boxplot()+
  geom_jitter()

BicAllmelt %>%
  mutate(group = reorder(model, BIC, median)) %>%
  ggplot(aes(x=BIC, y= model, height= ..density..))+
  geom_joy(scale=0.85)

# Count for how many participants a precise model was the best fit
BicAll$BestModel<-NA
for (j in 1: nrow(BicAll)){
  index<-which(BicAll[j,]==min(BicAll[j,2:(ncol(BicAll)-1)]))
  BicAll$BestModel[j]<-names(BicAll[index])
}


table(BicAll$BestModel)

ggplot(BicAll, aes(BestModel))+geom_bar()

# now loglikelihood
LL<-data.frame(matrix(NA, nrow=Sub, ncol=0))

# create number of subjects
LL$Sub<-seq(1:Sub)

for (name in modelname){
  LL[[name]]<-get(name)$BIC
}



LLmelt<-melt(LL, id.vars = "Sub")
names(LLmelt)<-c("Sub", "model", "LL")

LLMod<-lmer(LL~model+(1|Sub), data = LLmelt)
summary(LLMod)

# summary by model
LLmelt %>%
  group_by(model)   %>%
  summarize(Mean = mean(LL, na.rm=TRUE))

# plot them
p <- ggplot(LLmelt, aes(model, LL))
p+
  geom_violin()+
  geom_boxplot()+
  geom_jitter()

# Count for how many participants a precise model was the best fit
LL$BestModel<-NA
for (j in 1: nrow(LL)){
  index<-which(LL[j,]==max(LL[j,2:4]))
  LL$BestModel[j]<-names(LL[index])
}


table(LL$BestModel)

ggplot(LL, aes(BestModel))+geom_bar()


