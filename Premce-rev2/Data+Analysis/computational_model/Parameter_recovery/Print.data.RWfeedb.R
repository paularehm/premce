#------------------------------------------------------------------------------#
# Print graph for parameter recovery for RW model feedb

# Cerated: ""Wed Oct 20 17:47:06 2021"
#------------------------------------------------------------------------------#

rm(list=ls())

library(ggplot2)
library(gridExtra) # for plotting
library(ggpubr)

# models
models<-c("RWfeedbALL", "Bayesian2", "Bayesian_Nassar", "DualLR")

model<-models[1]

betalim<-10

name<- paste("output_files/parameterRecovery", model, ".", "betalimit=",  betalim,  
             ".initialQ=", 0.25 , sep="")

# retrieve all the files in the folder "cluster"
parameterRecov<-vector()
files<-list.files("computational_model/cluster/output_files",
                  pattern = "parameterRecoveryRWfeedbALL*")
for (n in 1: length(files)){
  currFile<-read.csv(paste0("computational_model/cluster/output_files/",files[n]))
  # append it
  parameterRecov<-rbind(parameterRecov, currFile)
}

# retrieve the file
#parameterRecov<-read.csv(paste0(name, ".csv"))

#parameterRecov<-parameterRecov[parameterRecov$fitBeta<20,]
# plot the alpha parameter
plotalpha<-ggplot(parameterRecov, aes(x=SimAlpha, y=fitAlpha)) + 
  geom_point()+
  geom_smooth(method=lm)+
  theme_bw()+
  stat_cor(method="pearson")+
  ggtitle("Alpha parameter")

plotalpha<-ggplot(parameterRecov, aes(x=SimAlpha, y=fitAlpha)) + 
  geom_point()+
  geom_smooth(method=lm)+
  theme_bw()+
  stat_cor(method="pearson")+
  ggtitle("Alpha parameter")

plotbeta<-ggplot(parameterRecov, aes(x=simBeta, y=fitBeta)) + 
  geom_point()+
  geom_smooth(method=lm)+
  theme_bw()+
  stat_cor(method="pearson")+
  #stat_cor(method = "pearson", label.x = 3, label.y = 30)+
  ggtitle("Beta parameter")
  
g<-grid.arrange(plotalpha, plotbeta,ncol=2)

arrangeGrob(plotalpha, plotbeta,ncol=2)

# save
ggsave(file=paste0("figures/ParameterRecovery_", model, "betalimit=", betalim,  ".jpg"), g)
       