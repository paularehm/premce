#!/bin/bash

#SBATCH --partition=general1
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=80
#SBATCH --mem-per-cpu=1024
#SBATCH --time=01:30:00
#SBATCH --no-requeue
#SBATCH --mail-type=ALL
#SBATCH --array 1-40

ID=$SLURM_ARRAY_TASK_ID

echo $ID

Rscript computational_model/cluster/Parameter.recovery_feedbALL.R $ID "0.1" 
Rscript computational_model/cluster/Parameter.recovery_feedbALL.R $ID "0.2" 
Rscript computational_model/cluster/Parameter.recovery_feedbALL.R $ID "0.3" 
Rscript computational_model/cluster/Parameter.recovery_feedbALL.R $ID "0.4" 
Rscript computational_model/cluster/Parameter.recovery_feedbALL.R $ID "0.5" 
Rscript computational_model/cluster/Parameter.recovery_feedbALL.R $ID "0.6" 
Rscript computational_model/cluster/Parameter.recovery_feedbALL.R $ID "0.7" 
Rscript computational_model/cluster/Parameter.recovery_feedbALL.R $ID "0.8" 
Rscript computational_model/cluster/Parameter.recovery_feedbALL.R $ID "0.9" 
Rscript computational_model/cluster/Parameter.recovery_feedbALL.R $ID "1" 
