
#############
########### recognition with individual change point created through cumulative accuracy
rm(list=ls())
library(dplyr)
library(ggplot2)
library(lme4)
library(car)
source("helper_functions/selPart.R")



# write the file
recogData<-read.csv( "output_files/recognitionData.csv")

# now I want personalised switch points
subjects<-unique(recogData$SubNum)
recogData$switchpoint<-NA

# check the graphs
#by list and participant
ggplot(recogData[recogData$listN!=0,], aes(x=NfromChange, y = cumAccBlock, color=iCP))+ geom_line()+
  facet_grid(listN~SubNum)


recogData$accuracy<-as.factor(recogData$accuracy)

recogData<-recogData[recogData$befAft!=2,]

# plot it # we are looking for an interaction between feedback and personalised-switch point
ggplot(recogData[recogData$listN!=2 ,], aes(x=iCP, y=recogAcc))+ geom_bar(aes(iCP, recogAcc, fill=iCP),
                                                                              position="dodge",stat="summary", fun.y="mean")+
  stat_summary(fun.data = "mean_cl_boot", size = 0.8, geom="errorbar", width=0.2 )+
  #geom_jitter( size=1,width=0.1)+
  theme(axis.text.x = element_blank())+ # we are showing the different levels through the colors, so we can avoid naming the bars
  theme(axis.ticks.x = element_blank())+
  theme_bw()+
  #facet_wrap(.~accuracy)+
  theme(strip.text.x = element_text(size = 13))+ 
  ylab(" % Hit")+
  # xlab("feedback")+
  ylim(c(0,1))+
  scale_fill_manual("legend", values = c("afterCP" = "darkgoldenrod1", "beforeCP" = "darkred"))+
  theme(axis.text.x = element_text( size = 20))+
  theme(axis.title=element_text(size=14,face="bold"))+
  theme(legend.position = "none")

# confidence
ggplot(recogData[recogData$listN!=2 ,], aes(x=iCP, y=confidence))+ geom_bar(aes(iCP, confidence, fill=iCP),
                                                                          position="dodge",stat="summary", fun.y="mean")+
  stat_summary(fun.data = "mean_cl_boot", size = 0.8, geom="errorbar", width=0.2 )+
  #geom_jitter( size=1,width=0.1)+
  theme(axis.text.x = element_blank())+ # we are showing the different levels through the colors, so we can avoid naming the bars
  theme(axis.ticks.x = element_blank())+
  theme_bw()+
  #facet_wrap(.~accuracy)+
  theme(strip.text.x = element_text(size = 13))+ 
  ylab(" % Hit")+
  # xlab("feedback")+
  #ylim(c(0,1))+
  scale_fill_manual("legend", values = c("afterCP" = "darkgoldenrod1", "beforeCP" = "darkred"))+
  theme(axis.text.x = element_text( size = 20))+
  theme(axis.title=element_text(size=14,face="bold"))+
  theme(legend.position = "none")

# is that significant?
switchpointacc<-glmer(recogAcc~switchpoint+(1|SubNum), family=binomial(), data= recogData)
summary(switchpointacc)

# does switchpoint explain additional variance compared to accuracy>
modacc<-glmer(recogAcc~accuracy+(1+accuracy|SubNum), family=binomial(), data= recogData)
summary(modacc)

modswitchpoint<-glmer(recogAcc~accuracy+switchpoint+(1+accuracy+switchpoint|SubNum), family=binomial(), data= recogData)
summary(modswitchpoint)

anova(modacc, modswitchpoint)


